/*
 * Minosoft
 * Copyright (C) 2020-2022 Moritz Zwerger
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * This software is not affiliated with Mojang AB, the original developer of Minecraft.
 */

package de.bixilon.minosoft.gui.eros.dialog.connection

import de.bixilon.minosoft.assets.AssetProgress
import de.bixilon.minosoft.gui.eros.controller.DialogController
import de.bixilon.minosoft.gui.eros.util.JavaFXUtil
import de.bixilon.minosoft.gui.eros.util.JavaFXUtil.text
import de.bixilon.minosoft.util.KUtil.toResourceLocation
import javafx.fxml.FXML
import javafx.scene.control.Button
import javafx.scene.control.ProgressBar
import javafx.scene.text.TextFlow

class VerifyAssetsDialog(
    val progress: AssetProgress,
) : DialogController() {
    @FXML private lateinit var headerFX: TextFlow
    @FXML private lateinit var countTextFX: TextFlow
    @FXML private lateinit var mibTextFX: TextFlow
    @FXML private lateinit var progressFX: ProgressBar
    @FXML private lateinit var cancelButtonFX: Button

    public override fun show() {
        JavaFXUtil.runLater {
            JavaFXUtil.openModal(TITLE, LAYOUT, this)
            progress.count += { JavaFXUtil.runLater { update() } }
            update()
            super.show()
        }
    }


    override fun init() {
        headerFX.text = HEADER
        cancelButtonFX.isDisable = true
    }

    private fun update() {
        val count = progress.count.count
        val total = progress.count.total
        val downloadedBytes = progress.downloadedSize.get()
        val totalBytes = progress.totalSize.get()
        if (count <= 0 && total > 0) {
            stage.close()
            return
        }
        countTextFX.text = "${total - count}/${total}"
        val percent = if (totalBytes <= 0) {
            0.0
        } else {
            downloadedBytes.toDouble() / totalBytes.toDouble()
        }
        val downloadedMB = downloadedBytes / 1_000_000.0
        val totalMB = totalBytes / 1_000_000.0
        mibTextFX.text = "%.2f MB / %.2f MB (%.2f%%)".format(downloadedMB, totalMB, percent * 100.0)
        progressFX.progress = percent
    }

    @FXML
    fun cancel() {
        TODO("Not yet implemented")
    }

    companion object {
        private val LAYOUT = "minosoft:eros/dialog/connection/verify_assets.fxml".toResourceLocation()

        private val TITLE = "minosoft:connection.dialog.verify_assets.title".toResourceLocation()
        private val HEADER = "minosoft:connection.dialog.verify_assets.header".toResourceLocation()
    }
}
