package de.bixilon.minosoft.assets

import de.bixilon.kutil.latch.CountUpAndDownLatch
import java.util.concurrent.atomic.AtomicLong

class AssetProgress (
    var count: CountUpAndDownLatch,
    var totalSize: AtomicLong,
    var downloadedSize: AtomicLong,
) {
    fun finishDownload(size: Long) {
        count.dec()
        downloadedSize.getAndAdd(size)
    }
}
